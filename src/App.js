import React from "react";
import "./App.css";
import KeywordExtractor from "./Components/KeywordExtractor";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>Nyckelordsutvinning</h1>
        <KeywordExtractor />
      </header>
    </div>
  );
}

export default App;
