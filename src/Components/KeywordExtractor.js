import React, { useState, useEffect } from "react";
import compromise from "compromise";
import stopWords from "../Script/stopWords.json"; // Importera stopporden från JSON-filen

function KeywordExtractor() {
  const [text, setText] = useState("");
  const [keywords, setKeywords] = useState([]);

  useEffect(() => {
    // Du kan placera här om du vill se innehållet i JSON-filen:
    // console.log(stopWords);
  }, []);

  const handleTextChange = (event) => {
    setText(event.target.value);
  };

  const extractKeywordsFromText = () => {
    const extractedKeywords = extractKeywords(text, 20);
    setKeywords(extractedKeywords);
  };

  const extractKeywords = (text, numKeywords) => {
    const doc = compromise(text);
    const terms = doc.terms();

    stopWords.forEach((stopWord) => {
      doc.remove(stopWord);
    });

    const keywords = terms
      .not(stopWords)
      .out("array")
      .filter((term) => term.split(" ").length === 1);

    if (numKeywords && numKeywords < keywords.length) {
      return keywords.slice(0, numKeywords);
    }

    return keywords;
  };

  return (
    <div className="KeywordExtractor">
      <textarea
        className="KeywordExtractor-textarea"
        rows="4"
        cols="50"
        placeholder="Skriv din text här..."
        value={text}
        onChange={handleTextChange}
      ></textarea>
      <button
        className="KeywordExtractor-button"
        onClick={extractKeywordsFromText}
      >
        Extrahera nyckelord
      </button>
      <div className="KeywordExtractor-keywords">
        {keywords.map((keyword, index) => (
          <span key={index} className="KeywordExtractor-keyword">
            #{keyword}
          </span>
        ))}
      </div>
    </div>
  );
}

export default KeywordExtractor;
